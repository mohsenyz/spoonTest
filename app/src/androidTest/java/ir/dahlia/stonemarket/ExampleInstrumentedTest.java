package ir.dahlia.stonemarket;

import android.Manifest;
import android.content.Context;
import android.os.Build;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.widget.LinearLayout;

import com.jraska.falcon.FalconSpoon;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import timber.log.Timber;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static ir.dahlia.stonemarket.utils.SlowSwipeActions.slowSwipeLeft;
import static ir.dahlia.stonemarket.utils.SlowSwipeActions.slowSwipeRight;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Rule
    public ActivityTestRule<HomeActivity> rule  = new  ActivityTestRule<>(HomeActivity.class, false);

    @Rule public GrantPermissionRule permissionRule = GrantPermissionRule.grant(Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE);


    @Test
    public void useAppContext() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("ir.dahlia.stonemarket", appContext.getPackageName());
    }


    private static void allowPermissionsIfNeeded() {
        if (Build.VERSION.SDK_INT >= 23) {
            UiDevice device = UiDevice.getInstance(getInstrumentation());
            UiObject allowPermissions = device.findObject(new UiSelector().text("Allow"));
            if (allowPermissions.exists()) {
                try {
                    allowPermissions.click();
                } catch (UiObjectNotFoundException e) {
                    Timber.e(e, "There is no permissions dialog to interact with ");
                }
            }
        }
    }

    @Test
    public void checkBottomTabExists() throws Exception {
        //Instrumentation.ActivityMonitor monitor = InstrumentationRegistry.getInstrumentation().addMonitor(HomeActivity.class.getName(), null, false);
        //InstrumentationRegistry.getInstrumentation().waitForMonitor(monitor);
        FalconSpoon.screenshot(rule.getActivity(), "Current-ViewPager-State");
        assertNotNull(onView(withId(R.id.bottom_tab)).check(matches(notNullValue())).check(matches(instanceOf(LinearLayout.class))));
    }

    @Test
    public void checkRtlViewPager() throws Exception {
        FalconSpoon.screenshot(rule.getActivity(), "Current-ViewPager-State");
        onView(withId(R.id.container)).perform(slowSwipeRight());
        onView(withText(containsString(": 2"))).check(matches(isDisplayed()));
        FalconSpoon.screenshot(rule.getActivity(), "Swiped-to-right");
        onView(withId(R.id.container)).perform(slowSwipeRight());
        onView(withText(containsString(": 3"))).check(matches(isDisplayed()));
        FalconSpoon.screenshot(rule.getActivity(), "Swiped-to-right");
        onView(withId(R.id.container)).perform(slowSwipeLeft());
        onView(withText(containsString(": 2"))).check(matches(isDisplayed()));
        FalconSpoon.screenshot(rule.getActivity(), "Swiped-to-left");
    }
}
