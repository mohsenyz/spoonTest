package ir.dahlia.stonemarket.utils;

import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.GeneralLocation;
import android.support.test.espresso.action.GeneralSwipeAction;
import android.support.test.espresso.action.Press;
import android.support.test.espresso.action.Swipe;

import static android.support.test.espresso.action.ViewActions.actionWithAssertions;

public class SlowSwipeActions {

    public static ViewAction slowSwipeLeft() {
        return actionWithAssertions(
                new GeneralSwipeAction(Swipe.SLOW, GeneralLocation.CENTER_RIGHT, GeneralLocation.CENTER_LEFT, Press.FINGER)
        );
    }

    public static ViewAction slowSwipeRight() {
        return actionWithAssertions(
                new GeneralSwipeAction(Swipe.SLOW, GeneralLocation.CENTER_LEFT, GeneralLocation.CENTER_RIGHT, Press.FINGER)
        );
    }

}
